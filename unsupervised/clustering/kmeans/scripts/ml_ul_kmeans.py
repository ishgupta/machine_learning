# In[]
import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
sys.path.insert(0, "/opt/Projects/data_integration")
from toolkit import get_config, get_data


# In[]
# config
config = {
    "data_file" : "data/clustering.csv",
    "clustering_var_k" : 3,
    "data_variables" :  [ "Loanamount", "Applicantincome" ]
}

# In[]
def find_closest_point( lst, K) : 
    return lst[ min( range( len( lst ) ), key = lambda i: abs( lst[ i ] - K ) ) ]

# In[]



# In[]
# read data
data = get_data( config["data_file"] )
data.head()

# In[]
# In[]
# extract X
X = data[ config[ "data_variables" ] ].copy( deep = True )

# clustering
k = config["clustering_var_k"]

# In[]
# visualize data points
plt.scatter( X["Applicantincome"], X["Loanamount"], c = 'black' )
plt.xlabel("Annual Income")
plt.ylabel("Loan Amount (in thousands)" )
plt.show()


# In[]
# plot data with centroids
centroids = ( X.sample( n = k ) )
plt.scatter( X["Applicantincome"], X["Loanamount"], c = 'black' )
plt.scatter( centroids['Applicantincome'], centroids['Loanamount'], c = 'red' )
plt.xlabel("Annual Income")
plt.ylabel("Loan amount (in thousands)" )
plt.show()

# In[]

# select random observations as centroids
centroids = X.sample( n = k )
centroids.reset_index( inplace = True, drop = True )

cluster_names = [ "Cluster_{0}".format( x + 1 ) for x in range( 0, k ) ]
cluster_distance_variables = [ "Distance_To_Cluster_{0}".format( x + 1 ) for x in range( 0, k ) ]

cluster_centroids_data = dict( ( cluster_var, centroid ) for cluster_var, centroid in zip( cluster_distance_variables, centroids.index ) )

# In[]
# 2 group points into the clusters as per their respective euclidian distance with the centroid

n_iterations = 1000
iter_index = 0

while True:
    print("iteration: #{0}".format( iter_index ))
    for cluster_var in cluster_distance_variables:
        if cluster_var not in X.columns:
            X[ cluster_var ] = None

    for cluster_var, centroid_index in cluster_centroids_data.items():
        X[ cluster_var ] = [
            round(
                np.sqrt( 
                    sum( 
                        [ 
                            ( centroids.loc[ centroid_index, col ] - X.loc[ idx, col ] ) ** 2 for col in centroids.columns 
                        ]  
                    )
                ), 4 
            ) 
            for idx in X.index
        ]

    X[ "Min_Cluster_Distance" ] = X[ cluster_distance_variables ].min( axis = 1 )
    X[ "Cluster" ] = None

    for cluster_var in cluster_distance_variables:
        X.loc[ X[ "Min_Cluster_Distance" ] == X[ cluster_var ], "Cluster" ] = cluster_names[ cluster_distance_variables.index( cluster_var ) ]

    new_centroids = X.groupby( [ "Cluster" ] ).mean()[ "Min_Cluster_Distance" ].reset_index().sort_values( "Cluster" )

    prev_centroids = centroids.copy( deep = True )
    centroids = pd.DataFrame( columns = config[ "data_variables" ] )

    for index, new_centroid in new_centroids.iterrows():
        centroids = pd.concat( [ centroids, 
                X.loc[ 
                    ( X[ "Cluster" ] == new_centroid[ "Cluster" ] ) & 
                    ( 
                        X[ "Min_Cluster_Distance" ] == find_closest_point( 
                            X.loc[ X[ "Cluster" ] == new_centroid[ "Cluster" ], "Min_Cluster_Distance" ].tolist(), 
                            new_centroid[ "Min_Cluster_Distance" ] 
                        )
                    ), config[ "data_variables" ]
                ].head(1)
            ]
        )
    centroids.reset_index( inplace = True, drop = True )
    iter_index += 1

    if ( ( ( centroids == prev_centroids ).sum()  == k ).all() ) or ( iter_index == n_iterations ):
        print( "final centroids found" )
        print( centroids )
        break

# In[]
# calculate inertia
X.rename(columns = { "Min_Cluster_Distance" : "Distance_To_Selected_Cluster" }, inplace = True)

inertia = X['Distance_To_Selected_Cluster'].sum().round(2)

# In[]
# calculate dunn index
centroids['Distance'] = None
for cluster_var, centroid_index in cluster_centroids_data.items():
    centroids['Distance'] = [
            round(
                np.sqrt( 
                    max( 
                        [ 
                            ( centroids.loc[ centroid_index, col ] - centroids.loc[ idx, col ] ) ** 2 for col in centroids.columns 
                        ]  
                    )
                ), 4 
            ) 
            for idx in centroids.index
        ]


# dunn_index = X['Distance_To_Selected_Cluster'].min() / 

# In[]
color = [ 'blue','green','cyan' ]

for tmp_k in range( k ):
    plot_data = X[ X[ "Cluster" ] == "Cluster_{0}".format( tmp_k + 1 ) ]
    plt.scatter( plot_data[ "Applicantincome" ], plot_data[ "Loanamount" ],c = color[ tmp_k ] )
plt.scatter( centroids[ "Applicantincome" ], centroids[ "Loanamount" ], c = 'red')
plt.xlabel( 'Income' )
plt.ylabel( 'Loan Amount (In Thousands)' )
plt.show()



# In[]
from sklearn.cluster import KMeans

X_ = data[ config[ "data_variables" ] ].copy( deep = True )

kmean_model = KMeans( n_clusters = config["clustering_var_k"], max_iter = 1000, precompute_distances=True, verbose = 1 )
kmean_model.fit( X_ )

kmean_model.cluster_centers_
kmean_model.inertia_
kmean_model.labels_

centroids

































# In[]
diff = 1
j = 0

while diff != 0:
    XD = X
    i = 1

    for index, row_c in centroids.iterrows():
        ED = []
        for index2, row_d in XD.iterrows():
            d1 = ( row_c[ "Applicantincome" ] - row_d[ "Applicantincome" ] ) ** 2
            d2 = ( row_c[ "Loanamount" ] - row_d["Loanamount"] ) ** 2
            d = np.sqrt( d1 + d2 )
            ED.append( d )
        X[ i ] = ED
        i = i + 1

    C = []
    for index, row in X.iterrows():
        min_dist = row[ 1 ]
        pos = 1
        for i in range( k ):
            if row[ i + 1 ] < min_dist:
                min_dist = row[ i + 1 ]
                pos = i + 1
            C.append( pos )
        X[ "Cluster" ] = C
        centroids_new = X.groupby( ["Cluster"] ).mean()[ [ "Loanamount", "Applicantincome" ] ]

        if j == 0:
            diff = 1
            j = j + 1
        else:
            diff = ( centroids_new[ "Loanamount" ] - centroids_new[ "Loanamount" ] ).sum() + \
                ( centroids_new[ "Applicantincome" ] - centroids_new[ "Applicantincome" ] ).sum()
            print( diff.sum() )

        centroids = X.groupby( ["Cluster"] ).mean()[ [ "Loanamount", "Applicantincome" ] ]
